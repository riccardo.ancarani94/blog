---
title: projects
date: 2020-03-07 13:40:45
---

* **BloodHound Playbooks** ([https://github.com/RiccardoAncarani/bloodhound-playbook](https://github.com/RiccardoAncarani/bloodhound-playbook))
Reproducible and extensible playbooks aimed at automating the analysis of Active Directory environments using the BloodHound tool. This project was successfully used in multiple client engagements and helped delivering more accurate results in less time. 
